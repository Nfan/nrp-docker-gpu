FROM nvidia/cuda:9.0-cudnn7-runtime-ubuntu16.04

ARG NRP_USER

ENV HOME /home/${NRP_USER}
ENV NRP_ROS_VERSION kinetic
ENV NRP_SOURCE_DIR /home/${NRP_USER}/nrp/src
ENV NRP_INSTALL_DIR /home/${NRP_USER}/.local
ENV HBP ${NRP_SOURCE_DIR}

RUN mkdir -p ${HOME} \
    ${NRP_SOURCE_DIR} \
    ${NRP_INSTALL_DIR} \
    ${NRP_SOURCE_DIR}/../platform_venv

#   Install prerequisites
RUN apt-get update \
    && apt-get install -y sudo \
    autoconf automake \
    build-essential cmake \
    gfortran \
    git \
    ipython \
    libgsl0-dev libhdf5-dev liblapack-dev libblas-dev libltdl7-dev libpq-dev libqt4-dev libtool libxslt1-dev \
    python-all-dev python-matplotlib python-numpy python-pip python-scipy python-virtualenv \
    libncurses5-dev \
    libreadline6-dev \
    ssh net-tools \
    curl libcurl3 php-curl \
    cython python-mpi4py \
    nano xvfb libxv1 \
    software-properties-common python-software-properties \
    libffi-dev \
    pkg-config \
    bison byacc libtool-bin \
    nano vim logrotate

WORKDIR ${HOME}/downloads
RUN wget --no-check-certificate downloads.sourceforge.net/project/virtualgl/2.5.1/virtualgl_2.5.1_amd64.deb \
    && dpkg -i virtualgl_2.5.1_amd64.deb \
    && sudo rm -rf virtualgl_2.5.1_amd64.deb

# add ros
RUN apt-get update \
    && apt-get install --no-install-recommends -y wget\
    software-properties-common python-software-properties \
    python-rosdep python-rosinstall python-vcstools \
    locales \
    && rm -rf /var/lib/apt/lists/*

# setup environment
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8

# setup keys
RUN apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 421C365BD9FF1F717815A3895523BAEEB01FA116

# setup sources.list
RUN echo "deb http://packages.ros.org/ros/ubuntu xenial main" > /etc/apt/sources.list.d/ros-latest.list

# install bootstrap tools
RUN apt-get update && apt-get install --no-install-recommends -y \
    python-rosdep \
    python-rosinstall \
    python-vcstools \
    && rm -rf /var/lib/apt/lists/*

# bootstrap rosdep
RUN rosdep init \
    && rosdep update

# install ros packages
RUN apt-get update && apt-get install -y \
    ros-${NRP_ROS_VERSION}-ros-core \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y \
    ros-${NRP_ROS_VERSION}-ros-base \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get update && \
    apt-get install -y --fix-missing ros-${NRP_ROS_VERSION}-control-toolbox \
    ros-${NRP_ROS_VERSION}-controller-manager \
    ros-${NRP_ROS_VERSION}-transmission-interface \
    ros-${NRP_ROS_VERSION}-joint-limits-interface \
    ros-${NRP_ROS_VERSION}-rosauth \
    ros-${NRP_ROS_VERSION}-smach-ros \
    ros-${NRP_ROS_VERSION}-rosauth \
    ros-${NRP_ROS_VERSION}-web-video-server
