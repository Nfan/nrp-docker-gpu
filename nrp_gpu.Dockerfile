FROM hbpneurorobotics/nrp:dev

ARG NRP_USER
ARG NRP_NUM_PROCESSES

USER root
ENV TENSORFLOW_ENV ${HOME}/.opt/tensorflow_venv
# Add tensorflow environment
# https://developer.humanbrainproject.eu/docs/projects/HBP%20Neurorobotics%20Platform/2.0/nrp/tutorials/tensorflow/tutorial.html#further-reading-a-more-complex-tensorflow-example-experiment
RUN apt-get update && apt-get install -y python-pip python-dev python-virtualenv unzip

USER ${NRP_USER}
# install protoc 3.4 to compile Object Detection API libraries
# https://github.com/tensorflow/models/issues/3995#issuecomment-382261558
WORKDIR ${HOME}/downloads
RUN virtualenv $TENSORFLOW_ENV \
  && . ${HOME}/.opt/tensorflow_venv/bin/activate \
  && easy_install -U pip \
  && pip install --upgrade tensorflow-gpu \
  && pip install "protobuf<=3.5.1" \
  && curl -L https://github.com/google/protobuf/releases/download/v3.4.0/protoc-3.4.0-linux-x86_64.zip -o protoc.zip \
  && unzip protoc.zip -d protoc34 \
  && cp protoc34/bin/protoc $TENSORFLOW_ENV/bin/

# install Tensorflow Object Detection API
# https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md
WORKDIR ${HOME}/.opt
RUN git clone https://github.com/tensorflow/models.git \
  && cd models/research \
  && . ${HOME}/.opt/tensorflow_venv/bin/activate \
  && pip install cython pillow lxml matplotlib \
  && protoc object_detection/protos/*.proto --python_out=.

# Download object detection models that return boxes as a result
COPY gpu/models.sh ./
RUN bash models.sh

WORKDIR $HBP
