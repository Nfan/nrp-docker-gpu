FROM ubuntu:xenial

ARG NRP_USER

ENV HOME /home/${NRP_USER}
ENV NRP_SOURCE_DIR /home/${NRP_USER}/nrp/src
ENV NRP_INSTALL_DIR /home/${NRP_USER}/.local
ENV HBP ${NRP_SOURCE_DIR}
ENV TERM linux
ENV ENVIRONMENT dev
ENV USER ${NRP_USER}

#   Get prerequisites
RUN apt-get update && \
    apt-get install -y sudo \
    curl nodejs npm \
    ruby-compass \
    git \
    nano psmisc \
    vim logrotate \
    iputils-ping netcat net-tools \
    nginx-extras \
    && rm -rf /var/lib/apt/lists/*
RUN gem install compass

RUN /bin/bash -c "mkdir -p $HOME/nginx && touch $HOME/nginx/nginx.pid"

#   Clone repos
WORKDIR ${NRP_SOURCE_DIR}
RUN git clone https://bitbucket.org/hbpneurorobotics/ExDFrontend \
    && git clone https://bitbucket.org/hbpneurorobotics/nrpBackendProxy \
    && git clone https://bitbucket.org/hbpneurorobotics/user-scripts


#   Install backend proxy
WORKDIR ${NRP_SOURCE_DIR}/nrpBackendProxy
RUN ln -s /usr/bin/nodejs /usr/bin/node
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
RUN /bin/bash -c "source $HOME/.nvm/nvm.sh && nvm install 0.10 \
    && nvm install 8 \
    && nvm alias default 8 \
    && nvm use default \
    && npm install \
    && cd ${NRP_SOURCE_DIR}/user-scripts && ./configure_nrp"

COPY ./config/bashrc $HOME/.bashrc

#   Install grunt
WORKDIR ${NRP_SOURCE_DIR}/ExDFrontend
RUN /bin/bash -c "source $HOME/.nvm/nvm.sh && nvm use default && npm install \
    && npm install -g grunt-cli \
    && npm install -g grunt"

#   Add user bbpnrsoa
RUN groupadd --gid 11860 bbp-ext
RUN useradd --home-dir ${HOME} --create-home --uid 901325 --gid 11860 --groups bbp-ext -ms /bin/bash ${NRP_USER}

RUN echo "bbpnrsoa ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

RUN rm -rf /var/lib/apt/lists/* \
    && apt-get update && apt-get install -y libgts-dev libjansson-dev

#   Setup supervisor
RUN apt-get update && apt-get install -y openssh-server apache2 supervisor \
    && mkdir -p /var/lock/apache2 /var/run/apache2 /var/run/sshd /var/log/supervisor \
    && chmod 766 /var/lock/apache2 /var/run/apache2 /var/run/sshd \
    && chmod 755 /var/log/supervisor \
    && mkdir /var/log/supervisor/nginx \
            /var/log/supervisor/proxy \
            /var/log/supervisor/frontend

ADD ./config/supervisord.f /etc/supervisord.d
ADD ./config/supervisord.conf /etc/supervisor/supervisord.conf
ADD ./config/default/supervisor /etc/default/supervisor

# Remove nrp-services.conf from nginx
# Send nginx output to stdout and stderr for supervisor
RUN rm -f ${HOME}/.local/etc/nginx/conf.d/nrp-services.conf \
    && sed -i "s/\$connection_upgrade/\'Upgrade\'/" ${HOME}/.local/etc/nginx/conf.d/frontend.conf \
    && sed -i "s/error_log.*/error_log \/dev\/stderr;/" ${HOME}/.local/etc/nginx/nginx.conf \
    && sed -i "s/access_log.*/access_log \/dev\/stdout custom;/" ${HOME}/.local/etc/nginx/nginx.conf

ADD ./config/logrotate.d/supervisor /etc/logrotate.d/supervisor


RUN chown -R ${NRP_USER}:bbp-ext ${HOME}
RUN nginx && killall nginx

USER ${NRP_USER}

# Install frontend
WORKDIR ${NRP_SOURCE_DIR}/ExDFrontend
RUN /bin/bash -c "source $HOME/.nvm/nvm.sh && nvm use default \
    && rm -rf node_modules \
    && npm install \
    && mkdir dist \
    && grunt build"

WORKDIR ${NRP_SOURCE_DIR}
