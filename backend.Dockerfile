FROM nrp_gazebo:dev

ARG NRP_USER
ARG NRP_NUM_PROCESSES

ENV USER ${NRP_USER}

#   Set environment vars
ENV C_INCLUDE_PATH=${NRP_INSTALL_DIR}/include:$C_INCLUDE_PATH \
    CPLUS_INCLUDE_PATH=${NRP_INSTALL_DIR}/include:$CPLUS_INCLUDE_PATH \
    CPATH=${NRP_INSTALL_DIR}/include:$CPATH \
    LD_LIBRARY_PATH=${NRP_INSTALL_DIR}/lib:$LD_LIBRARY_PATH \
    PATH=${NRP_INSTALL_DIR}/bin:$PATH \
    VIRTUAL_ENV=${HOME}/.opt/platform_venv

#   Install mvapich2
WORKDIR ${NRP_SOURCE_DIR}/mvapich2
RUN /bin/bash -c "./autogen.sh" \
    && ./configure --prefix=${NRP_INSTALL_DIR} --with-device=ch3:nemesis \
    && make -j${NRP_NUM_PROCESSES} \
    && make install


ENV PATH ${NRP_SOURCE_DIR}/MUSIC:$PATH
#   Install Music
WORKDIR ${NRP_SOURCE_DIR}/MUSIC
RUN apt-get install -y cython python-mpi4py \
    && ln -sf ${NRP_INSTALL_DIR}/bin/mpichversion mpich2version \
    && ./autogen.sh \
    && ./configure --prefix=${NRP_INSTALL_DIR} MPI_CXX=mpicxx\
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
    && rm -rf MUSIC


#   Install NEST
WORKDIR ${NRP_SOURCE_DIR}/nest-simulator
RUN virtualenv build_venv \
    && . build_venv/bin/activate \
    && pip install Cython==0.23.4 mpi4py==2.0.0 \
    && mkdir build && cd build \
    && cmake -DCMAKE_INSTALL_PREFIX:PATH=${NRP_INSTALL_DIR} -Dwith-gsl=ON -Dwith-mpi=ON -Dwith-music=ON .. \
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
    && cd ../SpikingCerebellum/src/CerebellumModule \
    && mkdir build && cd build && cmake .. && make && make install \
    && cd ../../../../build/ && cmake -DCMAKE_INSTALL_PREFIX:PATH=${NRP_INSTALL_DIR} -Dwith-gsl=ON -Dwith-mpi=ON -Dwith-music=ON -Dexternal-modules=cerebellum .. \
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
    && deactivate \
    && cd .. && rm -rf nest-simulator

#   Install experiement specific libraries
#   Install retina
WORKDIR ${NRP_SOURCE_DIR}/../_build
RUN qmake-qt4 ${NRP_SOURCE_DIR}/retina/retina.pro INSTALL_PREFIX=${NRP_SOURCE_DIR}/retina/build CONFIG+=release CONFIG+=nodisplay \
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
    && rm -rf ${NRP_SOURCE_DIR}/../_build

RUN pip install nengo

#   Install GazeboRosPackages
ENV CMAKE_PREFIX_PATH ${NRP_INSTALL_DIR}/lib/cmake/gazebo/:$CMAKE_PREFIX_PATH
WORKDIR ${NRP_SOURCE_DIR}/GazeboRosPackages
RUN rm -rf /var/lib/apt/lists/* \
    && apt-get update && apt-get install -y \
    ros-${NRP_ROS_VERSION}-sensor-msgs \
    ros-${NRP_ROS_VERSION}-angles \
    ros-${NRP_ROS_VERSION}-tf \
    ros-${NRP_ROS_VERSION}-image-transport \
    ros-${NRP_ROS_VERSION}-cv-bridge \
    ros-${NRP_ROS_VERSION}-control-toolbox \
    ros-${NRP_ROS_VERSION}-controller-manager \
    ros-${NRP_ROS_VERSION}-transmission-interface \
    ros-${NRP_ROS_VERSION}-joint-limits-interface \
    ros-${NRP_ROS_VERSION}-polled-camera \
    ros-${NRP_ROS_VERSION}-diagnostic-updater \
    ros-${NRP_ROS_VERSION}-rosbridge-server \
    ros-${NRP_ROS_VERSION}-camera-info-manager \
    ros-${NRP_ROS_VERSION}-xacro

ENV LD_LIBRARY_PATH ${NRP_INSTALL_DIR}/lib:$LD_LIBRARY_PATH
ENV CMAKE_PREFIX_PATH ${NRP_INSTALL_DIR}/lib/cmake/gazebo:${NRP_INSTALL_DIR}/lib/cmake/sdformat:$CMAKE_PREFIX_PATH
RUN /bin/bash -c "source ${NRP_INSTALL_DIR}/share/gazebo/setup.sh \
    && source /opt/ros/${NRP_ROS_VERSION}/setup.bash \
    && catkin_make"

RUN mkdir -p ~/.gazebo/models

#   Install node, nginx, and uwsgi
RUN apt-get -y install nodejs npm libgts-dev libjansson-dev imagemagick \
    nginx-extras lua-cjson uwsgi-plugin-python

COPY ./config/bashrc $HOME/.bashrc
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
RUN ln -s /usr/bin/nodejs /usr/bin/node
RUN /bin/bash -c "mkdir $HOME/nginx && touch $HOME/nginx/nginx.pid"

ENV TERM linux
ENV ENVIRONMENT dev
ENV PKG_CONFIG_PATH ${NRP_INSTALL_DIR}/lib/pkgconfig:/opt/ros/${NRP_ROS_VERSION}/lib/pkgconfig:${PKG_CONFIG_PATH}


RUN groupadd --gid 11860 bbp-ext
RUN useradd --home-dir ${HOME} --create-home --uid 901325 --gid 11860 --groups bbp-ext -ms /bin/bash ${NRP_USER}
RUN echo "bbpnrsoa ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

WORKDIR ${NRP_SOURCE_DIR}/gzweb
RUN /bin/bash -c "source $HOME/.nvm/nvm.sh && nvm install 0.10 \
    && nvm install 8 \
    && nvm alias default 8 \
    && nvm use default \
    && npm install -g bower \
    \
    && npm install && cd gz3d/utils && npm install && cd ../.. \
    \
    && export GAZEBO_MODEL_PATH=/home/bbpnrsoa/.gazebo/models \
    && ./deploy-gzbridge-nrp.sh \
    && ls -lah /home/bbpnrsoa/.gazebo/models \
    \
    && cd ${NRP_SOURCE_DIR}/user-scripts && ./configure_nrp  \
    \
    && python ${NRP_SOURCE_DIR}/user-scripts/generatelowrespbr.py"

WORKDIR ${NRP_SOURCE_DIR}/ExperimentControl
RUN export VIRTUAL_ENV=$VIRTUAL_ENV && make devinstall

WORKDIR ${NRP_SOURCE_DIR}/CLE
RUN export VIRTUAL_ENV=$VIRTUAL_ENV && make devinstall

WORKDIR ${NRP_SOURCE_DIR}/BrainSimulation
RUN export VIRTUAL_ENV=$VIRTUAL_ENV && make devinstall

RUN apt-get -y install python-dev python-h5py python-lxml \
    autogen \
    zlib1g-dev python-opencv \
    ruby libtar-dev libprotoc-dev protobuf-compiler \
    libtinyxml2-dev \
    libblas-dev \
    qt4-default libqtwebkit4 libqtwebkit-dev libfreeimage-dev libtbb-dev

WORKDIR ${NRP_SOURCE_DIR}/ExDBackend
RUN export VIRTUAL_ENV=$VIRTUAL_ENV &&  make devinstall

WORKDIR ${NRP_SOURCE_DIR}/VirtualCoach
RUN export VIRTUAL_ENV=$VIRTUAL_ENV && make devinstall
