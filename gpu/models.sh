for model in \
    ssd_mobilenet_v1_coco_2017_11_17 \
    ssd_mobilenet_v2_coco_2018_03_29 \
    ssdlite_mobilenet_v2_coco_2018_05_09 \
    ssd_inception_v2_coco_2017_11_17 \
    faster_rcnn_inception_v2_coco_2018_01_28 \
    faster_rcnn_resnet50_coco_2018_01_28 \
    faster_rcnn_resnet50_lowproposals_coco_2018_01_28 \
    rfcn_resnet101_coco_2018_01_28 \
    faster_rcnn_resnet101_coco_2018_01_28 \
    faster_rcnn_resnet101_lowproposals_coco_2018_01_28 \
    faster_rcnn_inception_resnet_v2_atrous_coco_2018_01_28 \
    faster_rcnn_inception_resnet_v2_atrous_lowproposals_coco_2018_01_28 \
    faster_rcnn_nas_coco_2018_01_28 \
    faster_rcnn_nas_lowproposals_coco_2018_01_28
  do \
    curl -OL http://download.tensorflow.org/models/object_detection/$model.tar.gz
    tar -xzf $model.tar.gz $model/frozen_inference_graph.pb
    cp -a $model ~/.opt/graph_def/
  done
ln -sf ~/.opt/graph_def/faster_rcnn_inception_resnet_v2_atrous_coco_2018_01_28/frozen_inference_graph.pb ~/.opt/graph_def/frozen_inference_graph.pb
