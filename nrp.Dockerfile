FROM nrp_backend:dev

ARG NRP_USER
ARG NRP_NUM_PROCESSES

#   Setup supervisor
RUN apt-get update && apt-get install -y openssh-server apache2 supervisor \
    && mkdir -p /var/lock/apache2 /var/run/apache2 /var/run/sshd /var/log/supervisor \
    && chmod 766 /var/lock/apache2 /var/run/apache2 /var/run/sshd \
    && chmod 755 /var/log/supervisor \
    && mkdir /var/log/supervisor/nginx \
            /var/log/supervisor/nrp-services_app \
            /var/log/supervisor/rosbridge \
            /var/log/supervisor/roscore \
            /var/log/supervisor/ros-simulation-factory_app \
            /var/log/supervisor/rosvideoserver \
            /var/log/supervisor/virtualcoach


ADD ./config/supervisord.d /etc/supervisord.d
ADD ./config/supervisord.conf /etc/supervisor/supervisord.conf
ADD ./config/default/supervisor /etc/default/supervisor

ADD ./config/logrotate.d/supervisor /etc/logrotate.d/supervisor

# Remove frontend.conf from nginx
# Send nginx output to stdout and stderr for supervisor
RUN rm -f ${HOME}/.local/etc/nginx/conf.d/frontend.conf \
    && sed -i "s/error_log.*/error_log \/dev\/stderr;/" ${HOME}/.local/etc/nginx/nginx.conf \
    && sed -i "s/access_log.*/access_log \/dev\/stdout custom;/" ${HOME}/.local/etc/nginx/nginx.conf

RUN chown -R ${NRP_USER}:bbp-ext ${HOME}
RUN nginx && killall nginx

USER ${NRP_USER}

RUN rosdep init | true
RUN rosdep update

WORKDIR $HBP
