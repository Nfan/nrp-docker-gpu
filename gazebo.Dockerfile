FROM nrp_base:dev

ARG NRP_USER
ARG NRP_NUM_PROCESSES

#   Set enviroment to build from bitbucket
ENV NRP_INSTALL_MODE user

#   Clone repos
COPY clone-repos.sh ${NRP_SOURCE_DIR}
WORKDIR ${NRP_SOURCE_DIR}
RUN git clone https://bitbucket.org/hbpneurorobotics/user-scripts && /bin/bash clone-repos.sh backend

# Uncomment the following block to checkout from specific date
#RUN ["/bin/bash", "-c", " \
#    cd ${NRP_SOURCE_DIR} ;\
#    for f in *; do \
#        if [ -d ${f} ]; then \
#            cd $f \
#            && git checkout `git rev-list -n 1 --before=\"${NRP_SOURCE_DATE}\" master` \
#            && cd ..; \
#        fi \
#    done "]

# Install Gazebo
RUN apt-get remove -y --purge gazebo6* \
    && wget -O - http://packages.osrfoundation.org/gazebo.key | apt-key add - \
    && apt-add-repository "deb http://packages.osrfoundation.org/gazebo/ubuntu xenial main"

RUN apt-get update && \
    apt-get install --no-install-recommends -y wget \
    libignition-math2-dev \
    libignition-transport-dev \
    libignition-transport0-dev \
    libboost-all-dev libtinyxml-dev libtinyxml2-dev ruby protobuf-compiler\
    && pip install psutil

#   Install sdformat
RUN apt-get remove -y *sdformat*
WORKDIR ${NRP_SOURCE_DIR}/../_build
RUN cmake -DCMAKE_INSTALL_PREFIX=${NRP_INSTALL_DIR} ${NRP_SOURCE_DIR}/sdformat \
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
#    && rm -rf ${NRP_SOURCE_DIR}/sdformat \
    && rm -rf ${NRP_SOURCE_DIR}/../_build

#   Install bulletphysics
WORKDIR ${NRP_SOURCE_DIR}/../_build
RUN cmake -DCMAKE_INSTALL_PREFIX=${NRP_INSTALL_DIR} ${NRP_SOURCE_DIR}/bulletphysics \
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
#    && rm -rf ${NRP_SOURCE_DIR}/bulletphysics \
    && rm -rf ${NRP_SOURCE_DIR}/../_build

ENV PKG_CONFIG_PATH ${NRP_INSTALL_DIR}/lib/pkgconfig:${NRP_INSTALL_DIR}/lib/x86_64-linux-gnu/pkgconfig:$PKG_CONFIG_PATH
ENV CMAKE_PREFIX_PATH ${NRP_INSTALL_DIR}/lib/x86_64-linux-gnu/cmake/gazebo:$CMAKE_PREFIX_PATH

WORKDIR ${NRP_SOURCE_DIR}/../_build
RUN cmake -DCMAKE_INSTALL_PREFIX=${NRP_INSTALL_DIR} ${NRP_SOURCE_DIR}/simbody \
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
    && rm -rf ${NRP_SOURCE_DIR}/../_build

WORKDIR ${NRP_SOURCE_DIR}/../opensim
RUN cmake -DCMAKE_INSTALL_PREFIX=${NRP_INSTALL_DIR} ${NRP_SOURCE_DIR}/opensim \
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
    && rm -rf ${NRP_SOURCE_DIR}/../_build

#   Gazebo
WORKDIR ${NRP_SOURCE_DIR}/../_build
RUN rm -rf /var/lib/apt/lists/* \
    && apt-get update && apt-get install -y libogre-1.9-dev xsltproc libqtwebkit-dev libfreeimage-dev \
    libtar-dev libprotoc-dev libtbb-dev libcurl4-openssl-dev
RUN cmake -DCMAKE_INSTALL_PREFIX=${NRP_INSTALL_DIR} -DENABLE_TESTS_COMPILATION:BOOL=False ${NRP_SOURCE_DIR}/gazebo \
    && make -j${NRP_NUM_PROCESSES} \
    && make install \
#    && rm -rf ${NRP_SOURCE_DIR}/gazebo \
    && rm -rf ${NRP_SOURCE_DIR}/../_build
