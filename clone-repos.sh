#!/bin/bash
# A script to clone all necessary repos when building Docker images.
# The $HBP environment variable needs to be set
# and $HBP/user-scripts must exist for this script to work successfully

if [[ -z $HBP ]] ; then
  echo "Please set the \$HBP variable to your desired installation path first."
  exit
else
  if [[ -d $HBP ]] ; then
    echo "The NRP repos will be cloned to the already-existing directory" $HBP
  elif [[ -f $HBP ]] ; then
    echo "Invalid destination." $HBP "is a file."
    exit 1
  else
    echo "The NRP repos will be cloned to the newly created directory" $HBP
    mkdir -p -v $HBP
  fi
fi

echo

function clone_repo {

  if [ "$#" -ne 1 ] ; then
    echo "Invalid number of arguments to clone_repo. Aborting."
    exit 1
  fi

  echo
  echo $1
  if [ -d "$1" ] ; then
    echo "Skipped, directory already exists."
  else
    git clone https://bitbucket.org/hbpneurorobotics/$1.git
  fi
  echo
}

option=$1
function help() {
  echo "Options: "
  echo "  backend: clone the NRP repositories required by the backend Docker image"
  echo "  frontend: clone NRP repositories required by the front-end Docker image"
  read option
}

source $HBP/user-scripts/repos.txt
nrp_backend=(
    ${nrp_python[*]}
    GazeboRosPackages
    user-scripts
    ${nrp_3rd_party[*]}
    gzweb
)
nrp_frontend=(
    nrpBackendProxy
    ExDFrontend
)
case $option in
backend)
  repos=(
    ${nrp_backend[*]}
  )
  ;;
frontend)
  repos=(
    ${nrp_frontend[*]}
  )
  ;;
*)
  help
  exit 0
  ;;
esac


pushd $HBP > /dev/null
for repo in ${repos[@]} ; do clone_repo $repo ; done
popd > /dev/null
